CC=gcc
CFLAGS=-Wall -O2
OBJECTS=strlcat.o strlcpy.o


all: teststr


teststr: test_str.c libbsdstr.a
	$(CC) $(CFLAGS) -o $@ $^


libbsdstr.a: $(OBJECTS)
	ar rcs $@ $^


%.o: %.c
	$(CC) -c $(CFLAGS) -o $@ $<

clean:
	rm -f *.a *.o teststr
