#include <stdio.h>

#include "strlcat.h"
#include "strlcpy.h"

#define BUFSIZE 64

static const char *phrase = "A quick brown fox jumps over a lazy dog.";

int
main()
{
  char buf[BUFSIZE];
  *buf = '\0';
  printf("== Testing strlcat ==\n");
  size_t total = strlcat(buf, "Hello", BUFSIZE);
  printf("Pass 1: output size = %lu\n", total);
  total = strlcat(buf, ", World!", BUFSIZE);
  printf("Pass 2: output size = %lu\n", total);
  printf("Result: %s\n", buf);
  printf("== Testing strlcpy ==\n");
  total = strlcpy(buf, phrase, BUFSIZE);
  printf("Pass 1: output size = %lu\n", total);
  printf("Result: %s\n", buf);
}
